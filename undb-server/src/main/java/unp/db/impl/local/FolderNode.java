
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.collection.ArraySequence;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.collection.Sequence;
import un.api.io.IOException;
import un.api.path.Path;
import unp.db.api.DBException;
import unp.db.api.DataNode;

/**
 *
 * @author Johann Sorel
 */
public class FolderNode extends AbstractLocalNode{

    private final Path folder;
    private final Sequence children = new ArraySequence();
    private boolean loaded = false;

    public FolderNode(Path folder, FolderNode parent) throws IOException {
        super(parent,parent.db.getNodeHandler(FolderNodeHandler.ID));
        this.folder = folder;
        //ensure folder exist
        folder.createContainer();
    }

    public FolderNode(LocalDataBase db) {
        super(db,db.getNodeHandler(FolderNodeHandler.ID));
        this.folder = db.getFolder();
    }

    public Path getFolder() {
        return folder;
    }

    public synchronized Sequence getChildren() throws IOException {
        if (!loaded) {
            loaded = true;
            final NodeHandler[] handlers = (NodeHandler[]) db.getNodeHandlers().toArray(NodeHandler.class);
            final Iterator ite = folder.getChildren().createIterator();

            loop:
            while (ite.hasNext()) {
                final Path child = (Path) ite.next();
                for (int i=0;i<handlers.length;i++) {
                    final DataNode node = handlers[i].create(this,child);
                    if(node!=null){
                        continue loop;
                    }
                }

                if (child.isContainer()) {
                    //create a folder node
                    new FolderNode(child,this);
                }
            }
        }
        return Collections.readOnlySequence(children);
    }

    public Chars getName() {
        if(parent==null) return Chars.EMPTY;
        return new Chars(folder.getName());
    }
    
    public void delete() throws DBException, IOException {
        if(parent==null) throw new DBException("Can not delete root node");
        folder.delete();
        parent.unregisterChild(this);
    }

    public void registerChild(DataNode child) {
        children.add(child);
    }

    public void unregisterChild(DataNode child) {
        children.remove(child);
    }

}
