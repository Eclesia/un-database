
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.collection.Collections;
import un.api.collection.Dictionary;
import un.api.collection.HashDictionary;
import un.api.collection.Iterator;
import un.api.collection.Set;
import un.api.io.IOException;
import un.api.path.Path;
import unp.db.api.DBException;
import unp.db.api.DBSession;
import unp.db.api.DataBase;
import unp.db.api.User;

/**
 *
 * @author Johann Sorel
 */
public class LocalDBSession implements DBSession {

    private final User user;
    private final Path folder;
    private final Dictionary databases = new HashDictionary();

    public LocalDBSession(Path folder, User user) throws DBException, IOException {
        this.user = user;
        this.folder = folder;
        final Iterator ite = folder.getChildren().createIterator();
        while (ite.hasNext()) {
            final Path p = (Path) ite.next();
            if (p.isContainer()) {
                final DataBase db = new LocalDataBase(this, p);
               databases.add(db.getName(), db);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public synchronized Set getDatabaseNames() throws DBException, IOException {
        return Collections.readOnlySet(databases.getKeys());
    }

    public synchronized DataBase createDatabase(Chars name) throws DBException, IOException {
        if (databases.getKeys().contains(name)) {
           throw new DBException("Database "+name+" already exist");
        }
        final Path path = folder.resolve(name);
        path.createContainer();
        final DataBase db = new LocalDataBase(this, path);
        databases.add(db.getName(), db);
        return db;
    }

    public synchronized void deletaDatabase(Chars name)  throws DBException, IOException {
        final LocalDataBase db = (LocalDataBase) databases.remove(name);
        if (db==null) {
            throw new DBException("Database "+name+" does not exist");
        } else {
            db.getFolder().delete();
        }
    }

    public synchronized DataBase getDatabase(Chars name) throws DBException, IOException {
        Object db = databases.getValue(name);
        if (db==null) {
            throw new DBException("Database "+name+" does not exist");
        }
        return (DataBase) db;
    }

}
