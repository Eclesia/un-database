
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.io.IOException;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.Document;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DBException;
import unp.db.api.DataBase;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.dto.DBDocuments;

/**
 *
 * @author Johann Sorel
 */
public abstract class RequestHandler {

    protected RequestType type;
    protected DataBase db;
    
    public RequestHandler(RequestType type) {
        this.type = type;
    }
    
    public void setDatabase(DataBase db) {
        this.db = db;
    }
    
    public RequestType getType() {
        return type;
    }

    public abstract boolean canSupport(DataNode node);
    
    public abstract Response execute(DataNode node, Request request) throws DBException, IOException;
    
    
    protected static Response simpleResponse(RequestType type, int code, Chars errorMessage) {
        final Document result = new DefaultDocument(false, DBDocuments.STATUS_TYPE);
        result.setFieldValue(new Chars("status"), code);
        if (errorMessage!=null) {
            result.setFieldValue(new Chars("errors"), Collections.singletonSequence(errorMessage));
        }

        return new Response(type, new Iterator[]{
            Collections.singletonIterator(result)
        });
    }
    
}
