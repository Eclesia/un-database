package unp.db.impl.local;

import unp.db.api.DBException;
import un.api.character.Chars;
import un.api.collection.Collection;
import un.api.collection.Dictionary;
import un.api.collection.HashDictionary;
import un.api.collection.Sequence;
import un.api.io.IOException;
import un.api.path.Path;
import un.api.predicate.FunctionResolver;
import un.api.predicate.Predicates;
import un.system.util.ModuleSeeker;
import unp.db.api.DataBase;
import unp.db.api.DataNode;

/**
 *
 * @author Johann Sorel
 */
public class LocalDataBase implements DataBase {

    private final LocalDBSession session;
    private final Path folder;
    private final FolderNode root;
    private final Chars name;
    private final Dictionary requestHandlers = new HashDictionary();
    private final Dictionary nodeHandlers = new HashDictionary();

    public LocalDataBase(LocalDBSession server, Path folder) throws DBException, IOException {
        this.session = server;
        this.folder = folder;
        this.name = new Chars(folder.getName());
        
        final Sequence requestHandlers;
        final Sequence nodeHandlers;
        try {
            requestHandlers = getRequestHandlers(this);
            nodeHandlers = getNodeHandlers(this);
        } catch (InstantiationException ex) {
            throw new DBException(ex);
        } catch (IllegalAccessException ex) {
            throw new DBException(ex);
        }
        
        for (int i=0,n=requestHandlers.getSize();i<n;i++) {
            final RequestHandler handler = (RequestHandler) requestHandlers.get(i);
            this.requestHandlers.add(handler.type.getId(), handler);
        }
        for (int i=0,n=nodeHandlers.getSize();i<n;i++) {
            final NodeHandler handler = (NodeHandler) nodeHandlers.get(i);
            this.nodeHandlers.add(handler.getId(), handler);
        }

        this.root = new FolderNode(this);
    }

    public Chars getName() {
        return name;
    }

    public LocalDBSession getSession() {
        return session;
    }

    public DataNode getRootNode() {
        return root;
    }

    Path getFolder() {
        return folder;
    }
    
    public RequestHandler getRequestHandler(Chars id) {
        return (RequestHandler) requestHandlers.getValue(id);
    }

    public Collection getRequestHandlers() {
        return requestHandlers.getValues();
    }

    public NodeHandler getNodeHandler(Chars id) {
        return (NodeHandler) nodeHandlers.getValue(id);
    }

    public Collection getNodeHandlers() {
        return nodeHandlers.getValues();
    }
    
    /**
     * Lists available request handlers
     * @return
     */
    private static Sequence getRequestHandlers(DataBase db) throws InstantiationException, IllegalAccessException{
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/unp/undb/unp.db.impl.local.RequestHandler"),
                Predicates.instanceOf(RequestHandler.class));
        for (int i=0,n=results.getSize();i<n;i++) {
            final RequestHandler handler = (RequestHandler) results.get(i).getClass().newInstance();
            handler.setDatabase(db);
            results.replace(i, handler);
        }
        return results;
    }

    /**
     * Lists available node handlers
     * @return
     */
    private static Sequence getNodeHandlers(DataBase db) throws InstantiationException, IllegalAccessException{
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/unp/undb/unp.db.impl.local.NodeHandler"),
                Predicates.instanceOf(NodeHandler.class));
        for (int i=0,n=results.getSize();i<n;i++) {
            final NodeHandler handler = (NodeHandler) results.get(i).getClass().newInstance();
            handler.setDatabase(db);
            results.replace(i, handler);
        }
        return results;
    }

    /**
     * Lists available function handlers
     * @return
     */
    private static Sequence getFunctionResolvers(DataBase db) throws InstantiationException, IllegalAccessException{
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/unp/undb/un.api.predicate.FunctionResolver"),
                Predicates.instanceOf(FunctionResolver.class));
        return results;
    }

}
