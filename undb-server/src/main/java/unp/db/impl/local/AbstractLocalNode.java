
package unp.db.impl.local;

import un.api.CObjects;
import un.api.character.Chars;
import un.api.collection.Collection;
import un.api.collection.Collections;
import un.api.collection.HashSet;
import un.api.collection.Iterator;
import un.api.collection.Set;
import un.api.io.IOException;
import unp.db.api.AbstractDataNode;
import unp.db.api.DBException;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractLocalNode extends AbstractDataNode {

    protected final FolderNode parent;
    protected final LocalDataBase db;
    protected final NodeHandler handler;

    public AbstractLocalNode(FolderNode parent,NodeHandler handler) {
        CObjects.ensureNotNull(parent);
        CObjects.ensureNotNull(handler);
        this.parent = parent;
        this.handler = handler;
        this.db = parent.getDataBase();
        parent.registerChild(this);
    }

    public AbstractLocalNode(LocalDataBase db,NodeHandler handler) {
        CObjects.ensureNotNull(db);
        CObjects.ensureNotNull(handler);
        this.parent = null;
        this.db = db;
        this.handler = handler;
    }

    public LocalDataBase getDataBase() {
        return db;
    }

    public DataNode getParent() {
        return parent;
    }

    public NodeHandler getHandler(){
        return handler;
    }

    public Chars getType() {
        return handler.getId();
    }
    
    public Collection getChildren() throws DBException, IOException {
        return Collections.emptyCollection();
    }

    public Set getRequestNames() throws DBException, IOException {
        final Set set = new HashSet();
        final Iterator ite = db.getRequestHandlers().createIterator();
        while(ite.hasNext()){
            RequestHandler handler = (RequestHandler)ite.next();
            if (handler.canSupport(this)){
               set.add(handler.type.getId());
            }
        }
        return set;
    }

    public RequestType getRequestType(Chars id) throws DBException, IOException {
        return db.getRequestHandler(id).getType();
    }

    public Response execute(Request request) throws DBException, IOException {
        if (request.getPath()!=null && !request.getPath().equals(getPathToRoot())) {
            throw new DBException("Request is for path "+request.getPath()+", current node is "+getPathToRoot());
        }
        final RequestHandler handler = (RequestHandler) db.getRequestHandler(request.getType().getId());
        if (handler==null) {
            throw new DBException("Unknown request : " + request.getType().getId());
        }
        return handler.execute(this,request);
    }

}
