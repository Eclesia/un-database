
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.collection.Collections;
import un.api.collection.HashSet;
import un.api.collection.Iterator;
import un.api.collection.Set;
import unp.db.api.AuthorizationSet;
import unp.db.api.User;

/**
 *
 * @author Johann Sorel
 */
public class LocalUser implements User{

    private final Chars login;
    private final Chars password;
    private final Set userAuths = new HashSet();
    private final Set userAuthSets = new HashSet();

    public LocalUser() {
        login = Chars.EMPTY;
        password = Chars.EMPTY;
    }

    public Chars getLogin() {
        return login;
    }

    public Chars getPassword() {
        return password;
    }

    public Set getAllAuthorisations() {
        final Set all = new HashSet();
        all.addAll(getUserAuthorisations());
        final Iterator authSets = getAuthorisationSets().createIterator();
        while(authSets.hasNext()){
            final AuthorizationSet authSet = (AuthorizationSet) authSets.next();
            all.addAll(authSet.getAuthorisations());
        }
        return all;
    }

    public Set getUserAuthorisations() {
        return Collections.readOnlySet(userAuths);
    }

    public Set getAuthorisationSets() {
        return Collections.readOnlySet(userAuthSets);
    }

}
