
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.io.IOException;
import un.api.path.Path;
import unp.db.api.DBException;
import unp.db.api.DataBase;
import unp.db.api.DataNode;

/**
 *
 * @author Johann Sorel
 */
public abstract class NodeHandler {

    protected final Chars id;
    protected DataBase db;

    public NodeHandler(Chars id) {
        this.id = id;
    }

    public void setDatabase(DataBase db) {
        this.db = db;
    }

    /**
     * Unique node type identifier.
     * 
     * @return id, never null
     */
    public Chars getId(){
        return id;
    }

    public abstract DataNode create(FolderNode parent, Path path) throws DBException, IOException;

}
