
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.io.IOException;
import un.api.path.Path;
import unp.db.api.DataNode;

/**
 *
 * @author Johann Sorel
 */
public class DocumentsNodeHandler extends NodeHandler{

    public static final Chars ID = new Chars("documents");

    public DocumentsNodeHandler() {
        super(ID);
    }

    public DataNode create(FolderNode parent, Path path) throws IOException {
        return new DocumentsNode(parent, path);
    }

}
