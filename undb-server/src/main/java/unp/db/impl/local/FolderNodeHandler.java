
package unp.db.impl.local;

import un.api.character.Chars;
import un.api.io.IOException;
import un.api.path.Path;
import unp.db.api.DataNode;

/**
 *
 * @author Johann Sorel
 */
public class FolderNodeHandler extends NodeHandler{

    public static final Chars ID = new Chars("folder");

    public FolderNodeHandler() {
        super(ID);
    }

    public DataNode create(FolderNode parent, Path path) throws IOException {
        return new FolderNode(path,parent);
    }

}
