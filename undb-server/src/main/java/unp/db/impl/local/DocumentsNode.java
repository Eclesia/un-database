
package unp.db.impl.local;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import un.api.character.Chars;
import un.api.collection.AbstractIterator;
import un.api.collection.Collection;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.collection.primitive.IntSequence;
import un.api.io.IOException;
import un.api.math.Maths;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.number.Int32;
import un.api.path.Path;
import un.api.predicate.Expression;
import un.api.predicate.Predicate;
import un.storage.binding.dbn.BinaryDocuments;
import un.storage.binding.dbn.DocumentReader;
import un.storage.binding.dbn.DocumentWriter;
import unp.db.dto.CollectionDoc;
import unp.db.dto.PathCollectionDoc;

/**
 *
 * @author Johann Sorel
 */
public class DocumentsNode extends AbstractLocalNode {

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final IntSequence blocks = new IntSequence();
    private int nextBlockId = 0;

    private final DocumentType docType;
    private final DocumentType colType;
    private final Path folder;
    private final Path docTypePath;

    /**
     * Open existing documents folder.
     *
     * @param parent
     * @param folder
     * @throws IOException
     */
    public DocumentsNode(FolderNode parent, Path folder) throws IOException {
        super(parent,parent.db.getNodeHandler(DocumentsNodeHandler.ID));
        this.folder = folder;
        this.docTypePath = folder.resolve(new Chars("doc.type"));

        //search for all blocks
        final Iterator ite = folder.getChildren().createIterator();
        while (ite.hasNext()) {
            Path p = (Path)ite.next();
            if(p.equals(docTypePath)) continue;
            int id = Int32.decode(p.getName());
            blocks.add(id);
            nextBlockId = Maths.max(nextBlockId, id+1);
        }

        final DocumentReader reader = new DocumentReader(BinaryDocuments.DOCTYPE);
        reader.setInput(docTypePath);
        final Document doc = reader.read();
        reader.dispose();
        docType = BinaryDocuments.toDocumentType(doc);
        colType = CollectionDoc.createCollectionType(docType);
    }

    /**
     * Create a new documents folder.
     * 
     * @param parent
     * @param type
     * @throws IOException
     */
    public DocumentsNode(FolderNode parent, DocumentType type) throws IOException {
        super(parent,parent.db.getNodeHandler(DocumentsNodeHandler.ID));
        final Chars id = type.getId();
        this.folder = parent.getFolder().resolve(id);
        if(this.folder.exists()) {
            throw new IOException("Type "+id+" already exist");
        }
        this.folder.createContainer();
        this.docTypePath = folder.resolve(new Chars("doc.type"));
        this.docType = type;

        final DocumentWriter writer = new DocumentWriter();
        writer.setOutput(docTypePath);
        writer.write(BinaryDocuments.toDocument(docType));
        writer.dispose();
        colType = CollectionDoc.createCollectionType(docType);
    }

    public Chars getName() {
        return docType.getId();
    }

    public DocumentType getDocumentType() {
        return docType;
    }

    public void delete() throws IOException {
        folder.delete();
        parent.unregisterChild(this);
    }

    public Iterator query(Expression[] exp, Predicate filter) throws IOException {
        try {
            lock.readLock().lock();
            return Collections.emptyIterator();
        } finally {
            lock.readLock().unlock();
        }
    }

    public Iterator createIterator() {
        try {
            //TODO lock ain't right, do a by-file lock
            lock.readLock().lock();
            final Iterator ite = new AbstractIterator() {
                final int[] array = blocks.toArrayInt();
                int i = 0;
                protected void findNext() {
                    if(i>=array.length) return;
                    final Path p = folder.resolve(Int32.encode(array[i]));
                    i++;
                    final PathCollectionDoc col = new PathCollectionDoc(docType, p);
                    next = col.createIterator();
                }
            };

            return Collections.serie(ite);
        } finally {
            lock.readLock().unlock();
        }
    }

    public void add(Collection documents) throws IOException {
        try {
            lock.writeLock().lock();
            final Chars id = Int32.encode(nextBlockId);
            nextBlockId++;
            final Path p = folder.resolve(id);
            final Document col = new DefaultDocument(colType);
            col.setFieldValue(CollectionDoc.FIELD_NAME, documents);

            final DocumentWriter writer = new DocumentWriter();
            writer.setOutput(p);
            writer.write(col);
            writer.dispose();
            blocks.add(nextBlockId-1);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void remove(Predicate filter) throws IOException {
        try {
            lock.writeLock().lock();

        } finally {
            lock.writeLock().unlock();
        }
    }


}
