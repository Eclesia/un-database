
package unp.db.impl.request;

import un.api.character.Chars;
import un.api.collection.Collection;
import un.api.io.IOException;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.storage.binding.dbn.BinaryFieldType;
import unp.db.api.DBException;
import unp.db.impl.local.DocumentsNode;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.FolderNode;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class AddDocuments extends RequestHandler {

    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true,
                    new FieldType[]{
                        BinaryFieldType.doc(new Chars("docs"), 1, 1, null, null, false, null),
                    }, null);
    
    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.AddDocuments"),
        new SectionType[]{ new SectionType(1, INPUT_TYPE) },
        new SectionType[]{ new SectionType(1, DBDocuments.STATUS_TYPE) });

    
    public AddDocuments() {
        super(TYPE);
    }

    public boolean canSupport(DataNode node) {
        return node instanceof FolderNode;
    }

    public Response execute(DataNode node, Request request) throws DBException, IOException {
        final Document parameters = (Document) request.nextSection().next();
        final Collection docs = (Collection) parameters.getFieldValue(new Chars("docs"));

        try {
            final DocumentsNode manager = (DocumentsNode) node;
            manager.add(docs);

        } catch(IOException ex) {
            return simpleResponse(TYPE, 2, new Chars(ex.getMessage()));
        }

        return simpleResponse(TYPE, 0, null);
    }


}
