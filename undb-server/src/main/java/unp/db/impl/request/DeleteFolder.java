
package unp.db.impl.request;

import un.api.character.CharEncodings;
import un.api.character.Chars;
import un.api.io.IOException;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import unp.db.api.DBException;
import un.storage.binding.dbn.BinaryDocuments;
import un.storage.binding.dbn.BinaryFieldType;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.FolderNode;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class DeleteFolder extends RequestHandler {

    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true,
                    new FieldType[]{
                        BinaryFieldType.text(new Chars("path"), 0, 1, null, CharEncodings.UTF_8, null)
                    }, null);
    
    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.DeleteFolder"),
        new SectionType[]{ new SectionType(1, INPUT_TYPE) },
        new SectionType[]{ new SectionType(1, DBDocuments.STATUS_TYPE) });

    
    public DeleteFolder() {
        super(TYPE);
    }

    @Override
    public boolean canSupport(DataNode node) {
        return node instanceof FolderNode;
    }

    public Response execute(DataNode node, Request request) throws DBException, IOException {
        final Document parameters = (Document) request.nextSection().next();
        final Chars path = (Chars) parameters.getFieldValue(new Chars("path"));

        DataNode candidate = node;
        if(path!=null){
            candidate = candidate.resolve(path);
        }
        try {
            ((FolderNode)candidate).delete();
        } catch(IOException ex) {
            return simpleResponse(TYPE, 2, new Chars(ex.getMessage()));
        }
        return simpleResponse(TYPE, 0, null);
    }


}
