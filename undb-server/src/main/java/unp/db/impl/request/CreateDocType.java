
package unp.db.impl.request;

import un.api.character.Chars;
import un.api.io.IOException;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.storage.binding.dbn.BinaryFieldType;
import unp.db.api.DBException;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.DocumentsNode;
import unp.db.impl.local.FolderNode;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class CreateDocType extends RequestHandler {

    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true,
                    new FieldType[]{
                        BinaryFieldType.doc(new Chars("type"), 1, 1, null, BinaryDocuments. DOCTYPE, false, null)
                    }, null);
    
    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.CreateDocType"),
        new SectionType[]{
            new SectionType(1, INPUT_TYPE)
        },
        new SectionType[]{
            new SectionType(1, DBDocuments.STATUS_TYPE)
        });

    public CreateDocType() {
        super(TYPE);
    }

    public boolean canSupport(DataNode node) {
        return node instanceof FolderNode;
    }

    public Response execute(DataNode node, Request request) throws DBException, IOException {
        final Document parameters = (Document) request.nextSection().next();
        final Document typeDoc = (Document) parameters.getFieldValue(new Chars("type"));
        final DocumentType docType = BinaryDocuments.toDocumentType(typeDoc);

        try {
            new DocumentsNode((FolderNode)node, docType);
        } catch(IOException ex) {
            return simpleResponse(TYPE, 2, new Chars(ex.getMessage()));
        }

        return simpleResponse(TYPE, 0, null);
    }


}
