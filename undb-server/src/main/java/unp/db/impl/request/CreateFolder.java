
package unp.db.impl.request;

import un.api.character.CharEncodings;
import un.api.character.Chars;
import un.api.io.IOException;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.api.path.Path;
import un.storage.binding.dbn.BinaryFieldType;
import unp.db.api.DBException;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.FolderNode;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class CreateFolder extends RequestHandler {

    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true,
                    new FieldType[]{
                        BinaryFieldType.text(new Chars("path"), 1, 1, null, CharEncodings.UTF_8, null)
                    }, null);
    
    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.CreateFolder"),
        new SectionType[]{ new SectionType(1, INPUT_TYPE) },
        new SectionType[]{ new SectionType(1, DBDocuments.STATUS_TYPE) });

    
    public CreateFolder() {
        super(TYPE);
    }

    @Override
    public boolean canSupport(DataNode node) {
        return node instanceof FolderNode;
    }

    public Response execute(DataNode node, Request request) throws DBException, IOException {
        final Document parameters = (Document) request.nextSection().next();
        final Chars path = (Chars) parameters.getFieldValue(new Chars("path"));

        final Chars[] segments = path.split('/');

        FolderNode parent = (FolderNode) node;
        for (int i=0;i<segments.length;i++) {
            try {
                try{
                    DataNode cdt = parent.getChild(segments[i]);
                    if (cdt instanceof FolderNode) {
                        //already exist
                        parent = (FolderNode) cdt;
                    } else {
                        //wrong type
                        throw new DBException("Node " + segments[i] + " already exist but is not a folder.");
                    }
                } catch (DBException ex) {
                    //Create node
                    final Path cp = parent.getFolder().resolve(segments[i]);
                    if (cp.exists()) {
                        throw new IOException("A file already exist for name "+segments[i]);
                    }
                    parent = new FolderNode(cp,parent);
                }

            } catch(IOException ex) {
                return simpleResponse(TYPE, 2, new Chars(ex.getMessage()));
            }
        }

        return simpleResponse(TYPE, 0, null);
    }


}
