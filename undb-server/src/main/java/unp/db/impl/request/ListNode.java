
package unp.db.impl.request;

import un.api.character.CharEncodings;
import un.api.character.Chars;
import un.api.collection.ArraySequence;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.collection.Sequence;
import un.api.io.IOException;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.DefaultDocumentType;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.storage.binding.dbn.BinaryFieldType;
import unp.db.api.DBException;
import un.storage.binding.dbn.BinaryDocuments;
import un.storage.binding.dbn.BinaryFieldValueType;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class ListNode extends RequestHandler {
    
    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true, new FieldType[]{
                    BinaryFieldType.numeric(new Chars("depth"), 0, 1, BinaryFieldValueType.TYPE_INT_LE, null, null)
            }, null);
    
    public static final DocumentType PATH_TYPE = new DefaultDocumentType(new Chars("Path"), null, null, true, new FieldType[]{
        BinaryFieldType.text(new Chars("path"), 1, 1, null, CharEncodings.UTF_8, null), 
        BinaryFieldType.text(new Chars("type"), 1, 1, null, CharEncodings.UTF_8, null)}, null);
    
    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.listNode"),
        new SectionType[]{new SectionType(1, INPUT_TYPE)}, 
        new SectionType[]{new SectionType(1, DBDocuments.STATUS_TYPE),
                          new SectionType(-1, PATH_TYPE)}
        );

    
    public ListNode() {
        super(TYPE);
    }

    public boolean canSupport(DataNode node) {
        return true;
    }

    public Response execute(DataNode node, Request request) throws DBException, IOException {
        final Document parameters = (Document) request.nextSection().next();
        final Chars path = (Chars) parameters.getFieldValue(new Chars("path"));

        final Sequence paths = new ArraySequence();
        listPath(db.getRootNode(), node, paths);
        final Document result = new DefaultDocument(false, DBDocuments.STATUS_TYPE);
        result.setFieldValue(new Chars("status"), 0);
        return new Response(TYPE, new Iterator[]{Collections.singletonIterator(result), paths.createIterator()});
    }

    private static void listPath(DataNode root, DataNode path, Sequence paths) throws DBException, IOException {
        final Chars relativePath = path.getPathToRoot();
        final Document pathDoc = new DefaultDocument(false, PATH_TYPE);
        pathDoc.setFieldValue(new Chars("path"), relativePath);
        pathDoc.setFieldValue(new Chars("type"), path.getType());
        if (path!=root) paths.add(pathDoc);

        //list children
        final Iterator ite = path.getChildren().createIterator();
        while (ite.hasNext()) {
            listPath(root, (DataNode) ite.next(), paths);
        }
    }

    
}
