
package unp.db.impl.request;

import un.api.character.Chars;
import un.api.io.IOException;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import unp.db.api.DBException;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.DocumentsNode;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class DeleteDocType extends RequestHandler {

    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true,
                    new FieldType[0], null);
    
    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.DeleteDocType"),
        new SectionType[]{
            new SectionType(1, INPUT_TYPE)
        },
        new SectionType[]{
            new SectionType(1, DBDocuments.STATUS_TYPE)
        });

    public DeleteDocType() {
        super(TYPE);
    }

    public boolean canSupport(DataNode node) {
        return node instanceof DocumentsNode;
    }
    
    public Response execute(DataNode node, Request request) throws DBException, IOException {
        try {
            ((DocumentsNode)node).delete();
        } catch(IOException ex) {
            return simpleResponse(TYPE, 2, new Chars(ex.getMessage()));
        }
        return simpleResponse(TYPE, 0, null);
    }

}
