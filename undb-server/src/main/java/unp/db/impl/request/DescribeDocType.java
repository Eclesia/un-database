
package unp.db.impl.request;

import un.api.character.Chars;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.io.IOException;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import unp.db.api.DBException;
import unp.db.impl.local.DocumentsNode;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DataNode;
import unp.db.api.Request;
import unp.db.api.RequestType;
import unp.db.api.Response;
import unp.db.api.SectionType;
import unp.db.dto.DBDocuments;
import unp.db.impl.local.RequestHandler;

/**
 *
 * @author Johann Sorel
 */
public class DescribeDocType extends RequestHandler {

    public static final DocumentType INPUT_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Parameters"), null, null, true,
                    new FieldType[0], null);

    public static final RequestType TYPE = new RequestType(new Chars("unp.db.impl.request.DescribeDocType"),
        new SectionType[]{
            new SectionType(1, INPUT_TYPE)
        },
        new SectionType[]{
            new SectionType(1, DBDocuments.STATUS_TYPE),
            new SectionType(1, BinaryDocuments.DOCTYPE)
        });

    public DescribeDocType() {
        super(TYPE);
    }

    public boolean canSupport(DataNode node) {
        return node instanceof DocumentsNode;
    }

    public Response execute(DataNode node, Request request) throws DBException, IOException {
        final DocumentsNode typePath = (DocumentsNode) node;
        final DocumentType docType = typePath.getDocumentType();
        final Document result = new DefaultDocument(false, DBDocuments.STATUS_TYPE);
        result.setFieldValue(new Chars("status"), 0);

        return new Response(TYPE, new Iterator[]{
            Collections.singletonIterator(result),
            Collections.singletonIterator(BinaryDocuments.toDocument(docType))
        });
    }

}
