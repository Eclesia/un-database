

package unp.db.local;

import static org.junit.Assert.*;
import org.junit.Test;
import un.api.character.Chars;
import un.api.io.IOException;
import un.api.path.Path;
import un.system.path.Paths;
import unp.db.api.DBException;
import unp.db.api.DBSession;
import unp.db.api.DataBase;
import unp.db.impl.local.LocalDBSession;
import unp.db.impl.local.LocalUser;

/**
 *
 * @author Johann Sorel
 */
public class LocalDataBaseTest {
    
    /**
     * Database creation test.
     */
    @Test
    public void createDBTest() throws IOException {
        final Chars dbName = new Chars("mybase");
        final Path root = Paths.resolve(new Chars("file:./target/dbtest"));
        if(root.exists()) root.delete();
        
        final DBSession session = new LocalDBSession(root,new LocalUser());
        
        //create database
        final DataBase db = session.createDatabase(dbName);
        assertEquals(1,session.getDatabaseNames().getSize());
        assertEquals(db,session.getDatabase(dbName));
        
        //create again, error expected
        try {
            session.createDatabase(dbName);
            fail("Creating an existing database should have failed");
        } catch(DBException ex){
            //ok
        }
        
        //delete database
        session.deletaDatabase(dbName);
        assertEquals(0,session.getDatabaseNames().getSize());
    }
    
}
