

package unp.db.local.request;

import un.api.character.Chars;
import un.api.io.IOException;
import un.api.path.Path;
import un.system.path.Paths;
import unp.db.api.DBSession;
import unp.db.api.DataBase;
import unp.db.impl.local.LocalDBSession;
import unp.db.impl.local.LocalUser;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractRequestTest {
    
    protected static DataBase createDB() throws IOException {
        final Chars dbName = new Chars("mybase");
        final Path root = Paths.resolve(new Chars("file:./target/dbtest"));
        if(root.exists()) root.delete();
        final DBSession session = new LocalDBSession(root,new LocalUser());
        final DataBase db = session.createDatabase(dbName);
        return db;
    }
}
