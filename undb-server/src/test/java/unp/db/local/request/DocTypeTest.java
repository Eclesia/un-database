

package unp.db.local.request;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import un.api.character.CharEncodings;
import un.api.character.Chars;
import un.api.collection.Iterator;
import un.api.io.IOException;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.DefaultDocumentType;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.storage.binding.dbn.BinaryFieldType;
import un.storage.binding.dbn.BinaryDocuments;
import unp.db.api.DataBase;
import unp.db.api.Request;
import unp.db.api.Response;
import static unp.db.local.request.AbstractRequestTest.createDB;
import unp.db.impl.request.CreateDocType;
import unp.db.impl.request.DeleteDocType;
import unp.db.impl.request.ListNode;

/**
 *
 * @author Johann Sorel
 */
public class DocTypeTest {
 
    @Test
    public void createTypeTest() throws IOException {
        
        final DataBase db = createDB();
        
        //check no path exist
        final Document listDoc = new DefaultDocument(false, ListNode.INPUT_TYPE);
        Response response = db.getRootNode().execute(new Request(ListNode.TYPE, listDoc));
        response.nextSection();
        Iterator ite = response.nextSection();
        assertEquals(false, ite.hasNext());
        
        
        //create type
        final FieldType[] fields = new FieldType[]{
            BinaryFieldType.text(new Chars("name"), 1, 1, null, CharEncodings.US_ASCII, null)
        };
        final DocumentType type = new DefaultDocumentType(new Chars("User"), null, null, true, fields, null);
        
        final Document createDoc = new DefaultDocument(false, CreateDocType.INPUT_TYPE);
        createDoc.setFieldValue(new Chars("type"), BinaryDocuments.toDocument(type));
        final Request create = new Request(CreateDocType.TYPE, createDoc);
        response = db.getRootNode().execute(create);
        assertEquals(0,((Document)response.nextSection().next()).getFieldValue(new Chars("status"))); //no error
        
        //check path created
        response = db.getRootNode().execute(new Request(ListNode.TYPE, listDoc));
        response.nextSection();
        ite = response.nextSection();
        assertEquals(true, ite.hasNext());
        Document p1Doc = (Document) ite.next();
        assertEquals(new Chars("User"), p1Doc.getFieldValue(new Chars("path")));
        assertEquals(new Chars("documents"), p1Doc.getFieldValue(new Chars("type")));
        assertEquals(false, ite.hasNext());
        
        //delete type
        final Document deleteDoc = new DefaultDocument(false, DeleteDocType.INPUT_TYPE);
        final Request delete = new Request(DeleteDocType.TYPE, deleteDoc);
        response = db.getRootNode().resolve(new Chars("User")).execute(delete);
        assertEquals(0,((Document)response.nextSection().next()).getFieldValue(new Chars("status"))); //no error
        
        //check type deleted
        response = db.getRootNode().execute(new Request(ListNode.TYPE, listDoc));
        response.nextSection();
        ite = response.nextSection();
        assertEquals(false, ite.hasNext());
    }
    
}
