
package unp.db.local.request;

import static org.junit.Assert.*;
import org.junit.Test;
import un.api.character.CharEncodings;
import un.api.character.Chars;
import un.api.collection.ArraySequence;
import un.api.collection.Collection;
import un.api.collection.Iterator;
import un.api.io.IOException;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.DefaultDocumentType;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.DocumentTypeBuilder;
import un.api.model.doc.FieldType;
import un.storage.binding.dbn.BinaryDocumentTypeBuilder;
import un.storage.binding.dbn.BinaryDocuments;
import un.storage.binding.dbn.BinaryFieldType;
import un.storage.binding.dbn.BinaryFieldValueType;
import unp.db.api.DataBase;
import unp.db.api.Request;
import unp.db.impl.local.DocumentsNode;
import unp.db.impl.request.CreateDocType;
import static unp.db.local.request.AbstractRequestTest.createDB;

/**
 *
 * @author Johann Sorel
 */
public class CRUDDocTest extends AbstractRequestTest {

    @Test
    public void addTest() throws IOException {

        final DataBase db = createDB();

        //create type
        final BinaryDocumentTypeBuilder builder = new BinaryDocumentTypeBuilder(new Chars("User"));
        builder.addField(new Chars("name")).type(BinaryFieldValueType.chars(CharEncodings.US_ASCII));
        builder.addField(new Chars("age")).type(BinaryFieldValueType.numeric(BinaryFieldValueType.TYPE_INT_LE));
        final DocumentType type = builder.build();

        final Document createDoc = new DefaultDocument(false, CreateDocType.INPUT_TYPE);
        createDoc.setFieldValue(new Chars("type"), BinaryDocuments.toDocument(type));
        db.getRootNode().execute(new Request(CreateDocType.TYPE, createDoc));

        //add documents
        final Document doc1 = new DefaultDocument(type);
        doc1.setFieldValue(new Chars("name"), new Chars("albert"));
        doc1.setFieldValue(new Chars("age"), 13);
        final Document doc2 = new DefaultDocument(type);
        doc2.setFieldValue(new Chars("name"), new Chars("maria"));
        doc2.setFieldValue(new Chars("age"), 42);
        final Document doc3 = new DefaultDocument(type);
        doc3.setFieldValue(new Chars("name"), new Chars("hans"));
        doc3.setFieldValue(new Chars("age"), 77);

        final Collection col = new ArraySequence(new Object[]{doc1,doc2,doc3});

        final DocumentsNode docsNode = (DocumentsNode) db.getRootNode().getChildren().createIterator().next();
        docsNode.add(col);

        //read documents
        final Iterator ite = docsNode.createIterator();
        assertTrue(ite.hasNext());
        assertEquals(doc1, ite.next());
        assertTrue(ite.hasNext());
        assertEquals(doc2, ite.next());
        assertTrue(ite.hasNext());
        assertEquals(doc3, ite.next());
        assertFalse(ite.hasNext());
    }

}
