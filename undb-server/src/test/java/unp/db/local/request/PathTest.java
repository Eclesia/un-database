
package unp.db.local.request;

import static org.junit.Assert.*;
import org.junit.Test;
import un.api.character.Chars;
import un.api.collection.Iterator;
import un.api.io.IOException;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.Document;
import unp.db.api.DataBase;
import unp.db.api.Request;
import unp.db.api.Response;
import unp.db.impl.request.CreateFolder;
import unp.db.impl.request.DeleteFolder;
import unp.db.impl.request.ListNode;

/**
 *
 * @author Johann Sorel
 */
public class PathTest extends AbstractRequestTest {
    
    @Test
    public void createPathTest() throws IOException {
        
        final DataBase db = createDB();
        
        //check no path exist
        final Document listDoc = new DefaultDocument(ListNode.INPUT_TYPE);
        Response response = db.getRootNode().execute(new Request(ListNode.TYPE, listDoc));
        response.nextSection();
        Iterator ite = response.nextSection();
        assertEquals(false, ite.hasNext());
        
        
        //create path
        final Document createDoc = new DefaultDocument(CreateFolder.INPUT_TYPE);
        createDoc.setFieldValue(new Chars("path"), new Chars("p1/p2/p3"));
        final Request create = new Request(CreateFolder.TYPE, createDoc);
        response = db.getRootNode().execute(create);
        assertEquals(0,((Document)response.nextSection().next()).getFieldValue(new Chars("status"))); //no error
        
        //check path created
        response = db.getRootNode().execute(new Request(ListNode.TYPE, listDoc));
        response.nextSection();
        ite = response.nextSection();
        assertEquals(true, ite.hasNext());
        Document p1Doc = (Document) ite.next();
        Document p2Doc = (Document) ite.next();
        Document p3Doc = (Document) ite.next();
        assertEquals(false, ite.hasNext());
        assertEquals(new Chars("p1"), p1Doc.getFieldValue(new Chars("path")));
        assertEquals(new Chars("p1/p2"), p2Doc.getFieldValue(new Chars("path")));
        assertEquals(new Chars("p1/p2/p3"), p3Doc.getFieldValue(new Chars("path")));
        assertEquals(new Chars("folder"), p1Doc.getFieldValue(new Chars("type")));
        assertEquals(new Chars("folder"), p2Doc.getFieldValue(new Chars("type")));
        assertEquals(new Chars("folder"), p3Doc.getFieldValue(new Chars("type")));
        
        //delete path
        final Document deleteDoc = new DefaultDocument(DeleteFolder.INPUT_TYPE);
        deleteDoc.setFieldValue(new Chars("path"), new Chars("./p1/p2"));
        final Request delete = new Request(DeleteFolder.TYPE, deleteDoc);
        response = db.getRootNode().execute(delete);
        assertEquals(0,((Document)response.nextSection().next()).getFieldValue(new Chars("status"))); //no error
        
        //check path deleted
        response = db.getRootNode().execute(new Request(ListNode.TYPE, listDoc));
        response.nextSection();
        ite = response.nextSection();
        assertEquals(true, ite.hasNext());
        p1Doc = (Document) ite.next();
        assertEquals(new Chars("p1"), p1Doc.getFieldValue(new Chars("path")));
        assertEquals(new Chars("folder"), p1Doc.getFieldValue(new Chars("type")));
        assertEquals(false, ite.hasNext());
    }
    
}
