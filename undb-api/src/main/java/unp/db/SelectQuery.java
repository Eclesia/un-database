
package unp.db;

import un.api.character.Chars;
import un.api.collection.Sequence;
import un.api.predicate.Expression;

/**
 *
 * @author Johann Sorel
 */
public class SelectQuery {

    private final Chars source;
    private Sequence fields;
    private Expression predicate;

    public SelectQuery(Chars source, Sequence fields, Expression predicate) {
        this.source = source;
        this.fields = fields;
        this.predicate = predicate;
    }

    public Chars getSource() {
        return source;
    }

    public Sequence getFields() {
        return fields;
    }

    public Expression getPredicate() {
        return predicate;
    }
    
}
