

package unp.db;

import un.api.array.Arrays;
import un.api.character.Chars;
import un.api.predicate.And;
import un.api.predicate.Expression;
import un.api.predicate.Function;
import un.api.predicate.FunctionResolver;
import un.api.predicate.Not;
import un.api.predicate.Or;
import un.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public class BaseFunctionResolver implements FunctionResolver {

    public Function resolve(Chars name, Expression[] parameters) {
        
        if (And.NAME.equals(name)) {
            final Predicate[] parts = new Predicate[parameters.length];
            Arrays.copy(parameters, 0, parameters.length, parts,0);
            return new And(parts);
        } else if (Or.NAME.equals(name)) {
            final Predicate[] parts = new Predicate[parameters.length];
            Arrays.copy(parameters, 0, parameters.length, parts,0);
            return new Or(parts);
        } else if (Not.NAME.equals(name)) {
            return new Not((Predicate) parameters[0]);
        } else{
            return null;
        }

    }
    
}
