
package unp.db.dto;

import un.api.character.CharEncodings;
import un.api.character.Chars;
import un.api.collection.ArraySequence;
import un.api.collection.Sequence;
import un.api.model.doc.DefaultDocument;
import un.api.model.doc.DefaultDocumentType;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.api.predicate.Constant;
import un.api.predicate.Expression;
import un.api.predicate.Function;
import un.api.predicate.FunctionResolver;
import unp.db.DocProperty;
import un.storage.binding.dbn.BinaryDocuments;
import static un.storage.binding.dbn.BinaryDocuments.SUBDOCTYPE;
import static un.storage.binding.dbn.BinaryDocuments.toDocumentType;
import un.storage.binding.dbn.BinaryFieldType;
import un.storage.binding.dbn.BinaryFieldValueType;
import unp.db.api.RequestType;
import unp.db.api.SectionType;

/**
 *
 * @author Johann Sorel
 */
public class DBDocuments {

    private static final Chars ATT_ID = new Chars("id");
    private static final Chars ATT_COUNT = new Chars("count");
    private static final Chars ATT_INPUTSECTIONS = new Chars("inputSections");
    private static final Chars ATT_OUTPUTSECTIONS = new Chars("outputSections");
    private static final Chars ATT_DOCTYPE = new Chars("docType");

    public static final DocumentType SECTIONTYPE;
    public static final DocumentType REQUESTTYPE;


    public static final DocumentType STATUS_TYPE = new DefaultDocumentType(new Chars("Status"), null, null, true, new FieldType[]{
        BinaryFieldType.numeric(new Chars("status"), 1, 1, BinaryFieldValueType.TYPE_INT_LE, null, null),
        BinaryFieldType.text(new Chars("infos"), 1, Integer.MAX_VALUE, null, CharEncodings.UTF_8, null),
        BinaryFieldType.text(new Chars("warnings"), 1, Integer.MAX_VALUE, null, CharEncodings.UTF_8, null),
        BinaryFieldType.text(new Chars("errors"), 1, Integer.MAX_VALUE, null, CharEncodings.UTF_8, null)
    }, null);


    public static final DocumentType EXPRESSION_TYPE;
    static {
        final FieldType[] fields = new FieldType[8];
        fields[0] = BinaryFieldType.bool(new Chars("b"), 0, 1, null, null);
        fields[1] = BinaryFieldType.numeric(new Chars("i"), 0, 1, BinaryFieldValueType.TYPE_INT_LE, null, null);
        fields[2] = BinaryFieldType.numeric(new Chars("l"), 0, 1, BinaryFieldValueType.TYPE_LONG_LE, null, null);
        fields[3] = BinaryFieldType.numeric(new Chars("d"), 0, 1, BinaryFieldValueType.TYPE_DOUBLE_LE, null, null);
        fields[4] = BinaryFieldType.text(new Chars("t"), 0, 1, null, CharEncodings.UTF_8, null);
        fields[5] = BinaryFieldType.text(new Chars("p"), 0, 1, null, CharEncodings.UTF_8, null);
        fields[6] = BinaryFieldType.text(new Chars("e"), 0, 1, null, CharEncodings.UTF_8, null);
        EXPRESSION_TYPE = new DefaultDocumentType(new Chars("Expression"), null, null, true, fields, null);
        fields[7] = BinaryFieldType.doc(new Chars("subs"), 0, Integer.MAX_VALUE, null, EXPRESSION_TYPE, false, null);
    }

    public static final DocumentType QUERY_FIELD_TYPE = new DefaultDocumentType(new Chars("QueryField"), null, null, true, new FieldType[]{
        BinaryFieldType.text(new Chars("name"), 1, 1, null, CharEncodings.UTF_8, null),
        BinaryFieldType.doc(new Chars("expression"), 1, 1, null, EXPRESSION_TYPE, false, null)
    }, null);

    public static final DocumentType SUBQUERY_TYPE = new DefaultDocumentType(new Chars("SubQuery"), null, null, true, new FieldType[]{
        BinaryFieldType.text(new Chars("name"), 1, 1, null, CharEncodings.UTF_8, null),
        BinaryFieldType.text(new Chars("src1"), 1, 1, null, CharEncodings.UTF_8, null),
        BinaryFieldType.text(new Chars("src2"), 0, 1, null, CharEncodings.UTF_8, null),
        BinaryFieldType.doc(new Chars("joint"), 0, 1, null, EXPRESSION_TYPE, false, null),
        BinaryFieldType.doc(new Chars("expressions"), 0, Integer.MAX_VALUE, null, QUERY_FIELD_TYPE, false, null),
        BinaryFieldType.doc(new Chars("predicate"), 0, 1, null, EXPRESSION_TYPE, false, null)
    }, null);

    public static final DocumentType QUERY_TYPE = new BinaryDocuments.BinaryDocumentType(new Chars("Query"), null, null, true, new FieldType[]{
            BinaryFieldType.doc(new Chars("subs"), 0, Integer.MAX_VALUE, null, SUBQUERY_TYPE, false, null)
            }, null);


    static {
        final BinaryFieldType fieldId = BinaryFieldType.text(ATT_ID, 1, 1, null, CharEncodings.UTF_8, null);

        final BinaryFieldType fieldCount = BinaryFieldType.numeric(ATT_COUNT, 1, 1, BinaryFieldValueType.TYPE_INT_LE, null, null);
        SECTIONTYPE = new BinaryDocuments.BinaryDocumentType(new Chars("SectionType"), null, null, true, new FieldType[]{
            fieldCount,
            BinaryFieldType.doc(ATT_DOCTYPE, 1, 1, null, SUBDOCTYPE, false, null)
            }, null);

        REQUESTTYPE = new BinaryDocuments.BinaryDocumentType(new Chars("RequestType"), null, null, true, new FieldType[]{
            fieldId,
            BinaryFieldType.doc(ATT_INPUTSECTIONS, 0, Integer.MAX_VALUE, null, SECTIONTYPE, false, null),
            BinaryFieldType.doc(ATT_OUTPUTSECTIONS, 0, Integer.MAX_VALUE, null, SECTIONTYPE, false, null)
            }, null);
    }


    public static SectionType toSectionType(Document doc){

        final int count = (Integer) doc.getFieldValue(ATT_COUNT);
        final Document reftype = (Document) doc.getFieldValue(ATT_DOCTYPE);
        final DocumentType docType = toDocumentType(reftype);

        return new SectionType(count, docType);
    }

    public static RequestType toRequestType(Document doc){

        final Chars id = (Chars) doc.getFieldValue(ATT_ID);

        final Document[] inputs = (Document[]) doc.getFieldValue(ATT_INPUTSECTIONS);
        final Document[] outputs = (Document[]) doc.getFieldValue(ATT_OUTPUTSECTIONS);
        final SectionType[] inputsections = new SectionType[inputs.length];
        final SectionType[] outputsections = new SectionType[outputs.length];
        for(int i=0;i<inputsections.length;i++){
            inputsections[i] = toSectionType(inputs[i]);
        }
        for(int i=0;i<outputsections.length;i++){
            outputsections[i] = toSectionType(outputs[i]);
        }

        return new RequestType(id, inputsections, outputsections);
    }

    public static Expression toExpression(Document doc, Sequence functionResolvers){
        Object value = doc.getFieldValue(new Chars("b"));
        if (value!=null) return new Constant(value);
        value = doc.getFieldValue(new Chars("i"));
        if (value!=null) return new Constant(value);
        value = doc.getFieldValue(new Chars("l"));
        if (value!=null) return new Constant(value);
        value = doc.getFieldValue(new Chars("d"));
        if (value!=null) return new Constant(value);
        value = doc.getFieldValue(new Chars("t"));
        if (value!=null) return new Constant(value);

        value = doc.getFieldValue(new Chars("p"));
        if (value!=null) return new DocProperty((Chars)value);

        Chars expName = (Chars) doc.getFieldValue(new Chars("e"));
        Sequence subs = (Sequence) doc.getFieldValue(new Chars("subs"));
        Expression[] exps = new Expression[subs.getSize()];
        for (int i=0,n= subs.getSize();i<n;i++) {
            exps[i] = toExpression((Document)subs.get(i),functionResolvers);
        }

        for (int i=0,n=functionResolvers.getSize();i<n;i++) {
            FunctionResolver r = (FunctionResolver) functionResolvers.get(i);
            Function f = r.resolve(expName, exps);
            if (f!=null) return f;
        }
        throw new IllegalArgumentException("Unknown function :"+expName);

    }

    public static Document toDocument(Expression exp) {
        final Document doc = new DefaultDocument(false, EXPRESSION_TYPE);

        if (exp instanceof Constant) {
            final Object value = exp.evaluate(null);
            if (value instanceof Boolean) {
                doc.setFieldValue(new Chars("b"), value);
            } else if (value instanceof Integer || value instanceof Short) {
                doc.setFieldValue(new Chars("i"), ((Number)value).intValue() );
            } else if (value instanceof Long) {
                doc.setFieldValue(new Chars("l"), value);
            } else if (value instanceof Float || value instanceof Double) {
                doc.setFieldValue(new Chars("d"), ((Number)value).doubleValue());
            } else if (value instanceof Chars) {
                doc.setFieldValue(new Chars("t"), value);
            } else {
                throw new IllegalArgumentException("Unsupported literal value "+value);
            }
        } else if (exp instanceof DocProperty) {
            doc.setFieldValue(new Chars("p"), ((DocProperty)exp).getPropertyName());
        } else if (exp instanceof Function) {
            final Function fct = (Function) exp;
            Expression[] parameters = fct.getParameters();
            final Sequence seq = new ArraySequence(parameters.length);
            for (int i=0,n=parameters.length;i<n;i++) {
                seq.add(toDocument(parameters[i]));
            }
            doc.setFieldValue(new Chars("e"), fct.getName());
            doc.setFieldValue(new Chars("subs"), seq);
        } else {
            throw new IllegalArgumentException("Unsupported expression "+exp);
        }

        return doc;
    }

}
