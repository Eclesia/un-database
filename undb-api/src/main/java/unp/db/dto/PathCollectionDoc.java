
package unp.db.dto;

import java.lang.reflect.Array;
import un.api.character.Chars;
import un.api.collection.AbstractCollection;
import un.api.collection.AbstractIterator;
import un.api.collection.Collection;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.collection.Set;
import un.api.io.IOException;
import un.api.model.doc.DocumentType;
import un.api.model.doc.Field;
import un.api.path.Path;
import un.storage.binding.dbn.BinaryStreamDocumentReader;

/**
 *
 * @author Johann Sorel
 */
public class PathCollectionDoc extends CollectionDoc {


    private final Path path;
    private final Collection docs = new AbstractCollection() {
        public boolean add(Object candidate) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public boolean remove(Object candidate) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public Iterator createIterator() {
            try {
                return new PathIte();
            } catch (IOException ex) {
                throw new RuntimeException(ex.getMessage(),ex);
            }
        }
    };

    public PathCollectionDoc(DocumentType elementType, Path path) {
        super(elementType);
        this.path = path;
    }

    public Collection getDocuments() {
        return docs;
    }

    public Set getFieldNames() {
        return Collections.singletonSet(FIELD_NAME);
    }

    public Field getField(Chars name) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public Object getFieldValue(Chars name) {
        if (FIELD_NAME.equals(name)) {
            return docs;
        }else{
            return null;
        }
    }

    public void setFieldValue(Chars name, Object value) {
        throw new UnsupportedOperationException("Not supported.");
    }


    public boolean add(Object candidate) {
        return docs.add(candidate);
    }

    public boolean addAll(Collection candidate) {
        return docs.addAll(candidate);
    }

    public boolean addAll(Object[] candidate) {
        return docs.addAll(candidate);
    }

    public boolean remove(Object candidate) {
        return docs.remove(candidate);
    }

    public boolean removeAll(Collection candidate) {
        return docs.removeAll(candidate);
    }

    public boolean removeAll(Object[] candidate) {
        return docs.removeAll(candidate);
    }

    public boolean removeAll() {
        return docs.removeAll();
    }

    public void replaceAll(Collection items) {
        docs.replaceAll(items);
    }

    public void replaceAll(Object[] items) {
        docs.replaceAll(items);
    }

    public boolean contains(Object candidate) {
        return docs.contains(candidate);
    }

    public Iterator createIterator() {
        return docs.createIterator();
    }

    public int getSize() {
        return docs.getSize();
    }

    public boolean isEmpty() {
        return docs.isEmpty();
    }

    public Object[] toArray() {
        return docs.toArray();
    }


    public Object[] toArray(Class clazz) {
        final int size = getSize();
        final Object[] array = (Object[]) Array.newInstance(clazz, size);
        if(size==0) return array;
        final Iterator ite = createIterator();
        for(int i=0;i<array.length;i++){
            array[i] = ite.next();
        }
        return array;
    }

    private class PathIte extends AbstractIterator {

        private final BinaryStreamDocumentReader reader = new BinaryStreamDocumentReader(docType);
        private boolean empty = false;

        private PathIte() throws IOException{
            reader.setInput(path);
            if(reader.next()!=BinaryStreamDocumentReader.TYPE_DOC_START){
                throw new IOException("Unvalid file");
            }
            if(reader.next()==BinaryStreamDocumentReader.TYPE_FIELD){

            }else{
                empty = true;
                reader.dispose();
            }
        }

        protected void findNext() {
            if (empty) return;

            try {
                next = reader.getFieldValue();
            } catch(IOException ex) {
                throw new RuntimeException(ex.getMessage(),ex);
            }
        }

        public void close() {
            try {
                reader.dispose();
            } catch (IOException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }

    }

}
