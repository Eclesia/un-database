
package unp.db.dto;

import un.api.character.Chars;
import un.api.collection.Collection;
import un.api.model.doc.AbstractDocument;
import un.api.model.doc.Document;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.storage.binding.dbn.BinaryDocuments;
import un.storage.binding.dbn.BinaryFieldType;

/**
 *
 * @author Johann Sorel
 */
public abstract class CollectionDoc extends AbstractDocument implements Collection,Document {

    public static final Chars FIELD_NAME = new Chars("docs");

    public static DocumentType createCollectionType(DocumentType elementType) {
        return new BinaryDocuments.BinaryDocumentType(new Chars("Collection"), null, null, true,
                new FieldType[]{BinaryFieldType.doc(FIELD_NAME, 0, Integer.MAX_VALUE, null, elementType, false, null)}, null);
    }

    protected CollectionDoc(DocumentType elementType) {
        super(createCollectionType(elementType));
    }

}
