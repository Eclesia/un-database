
package unp.db.api;

import un.api.character.Chars;
import un.api.collection.Iterator;
import un.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractDataNode implements DataNode{

    private static final Chars SELF_PATH = new Chars(".");
    private static final Chars PARENT_PATH = new Chars("..");

    private Chars pathToRoot;


    public Chars getPathToRoot() {
        if(pathToRoot==null) {
            DataNode parent = getParent();
            if(parent==null) {
                pathToRoot = Chars.EMPTY;
            } else {
                Chars pathToRoot = parent.getPathToRoot();
                if(pathToRoot.isEmpty()) {
                    this.pathToRoot = getName();
                } else {
                    this.pathToRoot = pathToRoot.concat('/').concat(getName());
                }
            }
        }
        return pathToRoot;
    }

    public DataNode getChild(Chars name) throws DBException,IOException {
        final Iterator ite = getChildren().createIterator();
        while (ite.hasNext()) {
            DataNode c = (DataNode) ite.next();
            if(c.getName().equals(name)){
                return c;
            }
        }
        throw new DBException("No node for name "+name);
    }

    public DataNode resolve(Chars ref) throws DBException,IOException {
        if(SELF_PATH.equals(ref) || ref.isEmpty()) return this;
        if(ref.startsWith(new Chars("./"))) ref = ref.truncate(2, -1);

        final Chars[] segments = ref.split('/');
        DataNode node = this;
        for (int i=0;i<segments.length;i++) {
            if(node==null) {
                throw new DBException("Unvalid path, a node does not exist");
            }
            if (SELF_PATH.equals(segments[i]) || Chars.EMPTY.equals(segments[i])) {
                node = this;
            } else if (PARENT_PATH.equals(segments[i])) {
                node = node.getParent();
            } else {
                node = node.getChild(segments[i]);
            }
        }

        if(node==null) {
            throw new DBException("Unvalid path, a node does not exist");
        }
        return node;
    }

}
