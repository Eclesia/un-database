
package unp.db.api;

import un.api.character.Chars;
import un.api.collection.Set;

/**
 *
 * @author Johann Sorel
 */
public interface User {

    Chars getLogin();

    Chars getPassword();

    Set getAllAuthorisations();

    Set getUserAuthorisations();

    Set getAuthorisationSets();

}
