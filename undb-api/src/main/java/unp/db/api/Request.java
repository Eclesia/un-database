
package unp.db.api;

import un.api.character.Chars;
import un.api.collection.Collections;
import un.api.collection.Iterator;
import un.api.model.doc.Document;


/**
 *
 * @author Johann Sorel
 */
public class Request {

    private final RequestType type;
    private Chars path;
    private Iterator[] sections;
    private int index = -1;

    public Request(RequestType type) {
        this.type = type;
    }

    /**
     * Shortcut constructor for requests with one section and one document input.
     *
     * @param type
     * @param doc
     */
    public Request(RequestType type, Document doc) {
        this.type = type;
        this.sections = new Iterator[]{
            Collections.singletonIterator(doc)
        };
    }

    /**
     *
     * @param type
     * @param sections iterator of documents for each section.
     */
    public Request(RequestType type, Iterator[] sections) {
        this.type = type;
        this.sections = sections;
    }

    public Chars getPath() {
        return path;
    }

    /**
     * @param path node path on which the request is executed.
     */
    public void setPath(Chars path) {
        this.path = path;
    }

    public void setSection(Document doc) {
        this.sections = new Iterator[]{
            Collections.singletonIterator(doc)
        };
    }

    public void setSections(Iterator[] sections) {
        this.sections = sections;
    }

    public RequestType getType() {
        return type;
    }
    
    /**
     * 
     * @return next section documents iterator or null if no more sections
     */
    public Iterator nextSection() {
        index++;
        return (index>=sections.length) ? null : sections[index];
    }
        
}
