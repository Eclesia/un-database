
package unp.db.api;

import un.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DBException extends IOException {

    public DBException() {
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(Throwable cause) {
        super(cause);
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
