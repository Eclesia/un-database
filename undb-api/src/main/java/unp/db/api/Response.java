
package unp.db.api;

import un.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public class Response {
    
    private final RequestType type;
    private final Iterator[] sections;
    private int index = -1;

    /**
     * 
     * @param type
     * @param sections iterator of documents for each section.
     */
    public Response(RequestType type, Iterator[] sections) {
        this.type = type;
        this.sections = sections;
    }

    public RequestType getType() {
        return type;
    }
    
    /**
     * 
     * @return next section documents iterator or null if no more sections
     */
    public Iterator nextSection() {
        index++;
        return (index>=sections.length) ? null : sections[index];
    }
        
}
