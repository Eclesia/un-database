
package unp.db.api;

import un.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public interface AuthorizationSet {

    Sequence getAuthorisations();

}
