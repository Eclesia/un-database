
package unp.db.api;

import un.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class RequestType {

    private final Chars id;
    private final SectionType[] inputsections;
    private final SectionType[] outputsections;

    public RequestType(Chars id, SectionType[] inputsections, SectionType[] outputsections) {
        this.id = id;
        this.inputsections = inputsections;
        this.outputsections = outputsections;
    }

    /**
     * Unique identifier of the request in the database.
     * 
     * @return request type id.
     */
    public Chars getId(){
        return id;
    }
    
    /**
     * Request input parts.
     * 
     * @return never null, if empty the request does not require any input.
     */
    public SectionType[] getInputSections(){
        return inputsections;
    }
    
    /**
     * Request response parts.
     * 
     * @return never null, if empty the request does not return any result.
     */
    public SectionType[] getOutputSections(){
        return outputsections;
    }

    public Request newInstance(){
        return new Request(this);
    }

    public Request newInstance(Chars path){
        final Request request = new Request(this);
        request.setPath(path);
        return request;
    }

}
