
package unp.db.api;

import un.api.character.Chars;
import un.api.collection.Set;
import un.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface DBSession {

    User getUser();

    DataBase createDatabase(Chars name) throws DBException, IOException;

    DataBase getDatabase(Chars name) throws DBException, IOException;

    void deletaDatabase(Chars name) throws DBException, IOException;

    Set getDatabaseNames() throws DBException, IOException;

}
