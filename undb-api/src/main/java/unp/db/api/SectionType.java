
package unp.db.api;

import un.api.model.doc.DocumentType;


/**
 * Responses can be composed of multiple parts.
 * Each part is composed of a single document type.
 * 
 * @author Johann Sorel
 */
public final class SectionType {
            
    private final int count;
    private final DocumentType docType;

    public SectionType(int count, DocumentType docType) {
        this.count = count;
        this.docType = docType;
    }

    /**
     * Number of documents in the respond part.
     * -1 : variable number of documents.
     * 
     * @return number of documents, -1 for variable.
     */
    public int getCount() {
        return count;
    }

    /**
     * Response part document type.
     * 
     * @return Document type, never null
     */
    public DocumentType getDocumentType() {
        return docType;
    }
    
}
