
package unp.db.api;

import un.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface DataBase {
    
    Chars getName();

    DBSession getSession();

    DataNode getRootNode();
    
}
