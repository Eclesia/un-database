
package unp.db.api;

import un.api.character.Chars;
import un.api.collection.Collection;
import un.api.collection.Set;
import un.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface DataNode {

    /**
     * Get node parent database.
     * 
     * @return DataBase never null.
     */
    DataBase getDataBase();

    /**
     * Get parent node.
     *
     * @return parent node, null if root node.
     */
    DataNode getParent();

    /**
     * Get node name.
     *
     * @return CharSequence node name
     */
    Chars getName();

    /**
     * Get node type.
     * A short type name used to differentiate nodes.
     *
     * @return CharSequence node type
     */
    Chars getType();

    /**
     * Get not absolute path from root.
     * This path is unique for each node.
     *
     * @return path, never null.
     */
    Chars getPathToRoot();

    /**
     *
     * @return Sequence of children nodes.
     * @throws unp.db.api.DBException
     * @throws IOException 
     */
    Collection getChildren() throws DBException, IOException;

    /**
     * Get child node with given name.
     * 
     * @param name child node name
     * @return DataNode, never null
     * @throws DBException
     * @throws IOException 
     */
    DataNode getChild(Chars name) throws DBException, IOException;

    /**
     * Resolve relative path to a DataNode.
     *
     * @param path
     * @return DataNode, never null
     * @throws DBException
     * @throws IOException
     */
    DataNode resolve(Chars path) throws DBException, IOException;

    /**
     * List possible requests for this node.
     *
     * @return
     * @throws DBException
     * @throws IOException
     */
    Set getRequestNames() throws DBException, IOException;

    /**
     * Describe a request.
     * 
     * @param id
     * @return
     * @throws DBException
     * @throws IOException
     */
    RequestType getRequestType(Chars id) throws DBException, IOException;
    
    /**
     * Execute request on this node.
     * 
     * @param request
     * @return
     * @throws DBException
     * @throws IOException 
     */
    Response execute(Request request) throws DBException, IOException;

}
