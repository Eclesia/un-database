
package unp.db;

import un.api.character.Chars;
import un.api.model.doc.Document;
import un.api.predicate.Expression;

/**
 *
 * @author Johann Sorel
 */
public class DocProperty implements Expression {

    private final Chars name;

    public DocProperty(Chars name) {
        this.name = name;
    }

    public Chars getPropertyName() {
        return name;
    }

    public Object evaluate(Object candidate) {
        if (candidate instanceof Document) {
            return ((Document)candidate).getFieldValue(name);
        } else {
            return null;
        }
    }

}
