
package unp.db;

import un.api.character.Chars;
import un.api.predicate.Expression;

/**
 *
 * @author Johann Sorel
 */
public class QueryField {
    
    private final Chars name;
    private final Expression expression;

    public QueryField(Chars name, Expression expression) {
        this.name = name;
        this.expression = expression;
    }

    public Chars getName() {
        return name;
    }

    public Expression getExpression() {
        return expression;
    }
    
}
