
package unp.db.dto;

import org.junit.Assert;
import org.junit.Test;
import un.api.character.Chars;
import un.api.collection.Collection;
import un.api.io.DataOutputStream;
import un.api.io.IOException;
import un.api.model.doc.DocumentType;
import un.api.model.doc.FieldType;
import un.api.number.NumberEncoding;
import un.api.path.Path;
import un.system.path.VirtualPath;
import un.storage.binding.dbn.BinaryDocuments;
import un.storage.binding.dbn.BinaryFieldType;
import un.storage.binding.dbn.BinaryFieldValueType;

/**
 *
 * @author Johann Sorel
 */
public class PathCollectionDocTest {

    @Test
    public void testCollection() throws IOException {

        //create an in memory file
        final Path path = new VirtualPath(new Chars("test.bin"));
        final DataOutputStream ds = new DataOutputStream(path.createOutputStream(), NumberEncoding.LITTLE_ENDIAN);
        ds.writeUByte(0);//document in stream mode
        ds.writeVarLengthInt(0); //number of documents = progressive
        ds.writeUByte(1);//doc 1
        ds.writeUByte(0);//document in stream mode
        ds.writeInt(1);
        ds.writeUByte(1);//doc 2
        ds.writeUByte(0);//document in stream mode
        ds.writeInt(2);
        ds.writeUByte(1);//doc 3
        ds.writeUByte(0);//document in stream mode
        ds.writeInt(3);
        ds.writeUByte(0);//end of list
        ds.close();

        final DocumentType docType = new BinaryDocuments.BinaryDocumentType(new Chars("type"),null,null,true,new FieldType[]{
            BinaryFieldType.numeric(new Chars("id"), 1, 1, BinaryFieldValueType.TYPE_INT_LE, null, null)
        },null);
        final PathCollectionDoc doc = new PathCollectionDoc(docType, path);
        final Collection documents = doc.getDocuments();
        Assert.assertEquals(3,documents.getSize());


    }

}
